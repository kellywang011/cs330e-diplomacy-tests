from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve


class TestDiplomacy(TestCase):
# ----
# solve
# ----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC Austin Support E\nD Berlin Move Austin\nE Houston Move FunkyTown")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE FunkyTown\n")
        


# ----
# main
# ----
if __name__ == "__main__":
    main()
""" #pragma: no cover
...
----------------------------------------------------------------------
Ran 3 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          57      0     32      0   100%
TestDiplomacy.py      21      0      0      0   100%
--------------------------------------------------------------
TOTAL                 78      0     32      0   100%
"""
